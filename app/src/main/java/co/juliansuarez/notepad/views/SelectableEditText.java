package co.juliansuarez.notepad.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;

import java.util.HashSet;
import java.util.Set;

import co.juliansuarez.notepad.R;

/**
 * Created by Julián on 23/11/2015.
 */
public class SelectableEditText extends EditText {

    public static final char CHECKBOX_UNMARKED = '☐';
    public static final char CHECKBOX_MARKED = '☑';

    private Set<SelectionChangedListener> listeners;

    public SelectableEditText(Context context) {
        this(context, null);
    }

    public SelectableEditText(Context context, AttributeSet attrs) {
        this(context, attrs, R.attr.editTextStyle);
    }

    public SelectableEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        listeners = new HashSet<>();
    }

    @Override
    protected void onSelectionChanged(int selStart, int selEnd) {
        super.onSelectionChanged(selStart, selEnd);
        if (listeners != null) {
            for (SelectionChangedListener listener : listeners) {
                listener.onSelectionChanged(selStart, selEnd);
            }
        }
    }

    public void addSelectionChangedListener(SelectionChangedListener selectionChangedListener) {
        listeners.add(selectionChangedListener);
    }

    public void removeSelectionChangedListener(SelectionChangedListener selectionChangedListener) {
        listeners.remove(selectionChangedListener);
    }

    public interface SelectionChangedListener {
        void onSelectionChanged(int selStart, int selEnd);
    }
}
