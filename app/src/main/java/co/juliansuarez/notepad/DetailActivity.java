package co.juliansuarez.notepad;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.CheckBox;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.juliansuarez.notepad.data.NoteManager;
import co.juliansuarez.notepad.model.Note;
import co.juliansuarez.notepad.views.SelectableEditText;

public class DetailActivity extends AppCompatActivity implements SelectableEditText.SelectionChangedListener {

    private MODE mode;
    private Note note;

    @Bind(R.id.content)
    SelectableEditText content;

    @Bind(R.id.buttonApply)
    Button buttonApply;

    @Bind(R.id.favorite)
    CheckBox favorite;

    private boolean ignoreNextPass = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        initData();
        initViews();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.detail_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_add_checkbox) {
            content.append("\n" + SelectableEditText.CHECKBOX_UNMARKED + " ");
        } else if (item.getItemId() == R.id.menu_delete) {

        }
        return super.onOptionsItemSelected(item);
    }

    private void initViews() {
        ButterKnife.bind(this);
        buttonApply.setText(mode == MODE.ADD ? getString(R.string.add_button_label) : getString(R.string.update_button_label));
        if (!TextUtils.isEmpty(note.getContent())) {
            content.setText(note.getContent());
        }

        favorite.setChecked(note.isFavorite());

        content.addSelectionChangedListener(this);
    }

    @OnClick(R.id.buttonApply)
    public void onApplyButtonClicked() {
        note.setContent(content.getText().toString());
        note.setIsFavorite(favorite.isChecked());
        NoteManager noteManager = NoteManager.getInstance();
        noteManager.saveNote(note);
        setResult(RESULT_OK);
        finish();
    }

    private void initData() {
        if (getIntent().getExtras() == null) {
            mode = MODE.ADD;
            note = new Note("");
        } else {
            mode = MODE.EDIT;
            note = (Note) getIntent().getExtras().getSerializable(Note.class.getSimpleName());
        }
    }

    @Override
    public void onSelectionChanged(int selStart, int selEnd) {
        final StringBuilder text = new StringBuilder(content.getText().toString());
        // Make sure we are only selecting one character
        if (!ignoreNextPass) {
            if (selEnd - selStart <= 1 && selStart < text.length()) {
                final char selectedCharacter = text.charAt(selStart);
                Log.d(DetailActivity.class.getSimpleName(), "selStart=" + selStart + " selEnd" + selEnd + " One char selected" + text + " text=" + text.substring(selStart, selStart + 1));
                if (selectedCharacter == SelectableEditText.CHECKBOX_MARKED) {
                    text.setCharAt(selStart, SelectableEditText.CHECKBOX_UNMARKED);
                    ignoreNextPass = true;
                    content.setText(text.toString());
                } else if (selectedCharacter == SelectableEditText.CHECKBOX_UNMARKED) {
                    text.setCharAt(selStart, SelectableEditText.CHECKBOX_MARKED);
                    ignoreNextPass = true;
                    content.setText(text.toString());
                }
            }
        } else {
            ignoreNextPass = false;
        }

    }

    public enum MODE {
        EDIT, ADD
    }
}
