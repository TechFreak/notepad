package co.juliansuarez.notepad.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import co.juliansuarez.notepad.R;
import co.juliansuarez.notepad.model.Note;
import co.juliansuarez.notepad.views.SelectableEditText;

/**
 * Created by Julián on 18/11/2015.
 */
public class NotesAdapter extends RecyclerView.Adapter<NotesAdapter.NotesViewHolder> {

    private final Callback callback;
    private List<Note> notes;

    public NotesAdapter(List<Note> notes, Callback callback) {
        this.notes = notes;
        this.callback = callback;

    }

    public void setNotes(List<Note> notes) {
        this.notes = new ArrayList<>(notes);
        notifyDataSetChanged();
    }

    public void addNote(Note note) {
        this.notes.add(note);
        notifyDataSetChanged();
    }

    @Override
    public NotesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final Context context = parent.getContext();
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        final View view = layoutInflater.inflate(R.layout.note_item, parent, false);
        return new NotesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(NotesViewHolder holder, final int position) {
        final Note note = notes.get(position);
        String content = note.getContent();
        content = content.replaceAll(SelectableEditText.CHECKBOX_MARKED + "", " ");
        content = content.replaceAll(SelectableEditText.CHECKBOX_UNMARKED + "", " ");
        content = content.replaceAll("\n", " ");
        if (content.length() > 20) {
            content = content.substring(0, 20);
        }
        holder.text.setText(content);
        holder.text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (callback != null) {
                    callback.onEditNote(note);
                }
            }
        });

        if (note.isFavorite()) {
            holder.text.setCompoundDrawablesWithIntrinsicBounds(android.R.drawable.star_on, 0, 0, 0);
        } else {
            holder.text.setCompoundDrawablesWithIntrinsicBounds(android.R.drawable.star_off, 0, 0, 0);
        }
    }

    @Override
    public int getItemCount() {
        return notes.size();
    }

    public interface Callback {
        void onEditNote(Note note);
    }

    class NotesViewHolder extends RecyclerView.ViewHolder {
        TextView text;

        public NotesViewHolder(View itemView) {
            super(itemView);
            text = (TextView) itemView;
        }
    }
}
