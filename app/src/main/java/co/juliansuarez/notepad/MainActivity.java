package co.juliansuarez.notepad;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.juliansuarez.notepad.adapter.NotesAdapter;
import co.juliansuarez.notepad.data.NoteManager;
import co.juliansuarez.notepad.data.network.Api;
import co.juliansuarez.notepad.data.network.model.ExternalNote;
import co.juliansuarez.notepad.model.Note;
import retrofit2.Callback;
import retrofit2.GsonConverterFactory;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MainActivity extends AppCompatActivity implements TextWatcher, NotesAdapter.Callback {

    private static final int ADD_OR_EDIT_REQUEST_CODE = 1;

    @Bind(R.id.search_term)
    EditText search_term;

    @Bind(R.id.notes_list)
    RecyclerView notes_list;

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    NotesAdapter notesAdapter;

    NoteManager noteManager;

    Api service;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://notepad-backend-twilio.herokuapp.com")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        service = retrofit.create(Api.class);
        fetchExternalNotes();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_refresh) {
            fetchExternalNotes();
        }
        return super.onOptionsItemSelected(item);
    }

    private void initViews() {
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        notes_list.setLayoutManager(new LinearLayoutManager(this));

        noteManager = NoteManager.getInstance();
        notesAdapter = new NotesAdapter(noteManager.getNotes(), this);

        notes_list.setAdapter(notesAdapter);

        search_term.addTextChangedListener(this);
    }

    @OnClick(R.id.fab)
    public void onAddButtonClicked() {
        startActivityForResult(new Intent(MainActivity.this, DetailActivity.class), ADD_OR_EDIT_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ADD_OR_EDIT_REQUEST_CODE && resultCode == RESULT_OK) {
            final List<Note> notes = noteManager.getNotes();
            notesAdapter.setNotes(notes);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        final List<Note> searchResults = noteManager.search(s.toString());
        notesAdapter.setNotes(searchResults);
    }

    @Override
    public void onEditNote(Note note) {
        Intent detail = new Intent(MainActivity.this, DetailActivity.class);
        detail.putExtra(Note.class.getSimpleName(), note);
        startActivityForResult(detail, ADD_OR_EDIT_REQUEST_CODE);
    }

    private void fetchExternalNotes() {
        service.getItems().enqueue(new Callback<List<ExternalNote>>() {
            @Override
            public void onResponse(Response<List<ExternalNote>> response) {
                for (ExternalNote item : response.body()) {
                    Note note = new Note(item.getContent());
                    note.setExternalId(Long.parseLong(item.getId()));
                    noteManager.saveNote(note);
                }
                notesAdapter.setNotes(noteManager.getNotes());
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });
    }
}
