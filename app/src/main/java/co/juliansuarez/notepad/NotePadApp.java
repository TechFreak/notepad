package co.juliansuarez.notepad;

import android.app.Application;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import co.juliansuarez.notepad.data.NoteManager;

/**
 * Created by Julián on 18/11/2015.
 */
public class NotePadApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        SharedPreferences sharedPreferences = getSharedPreferences(NoteManager.NOTES_PREFERENCES, MODE_PRIVATE);
        Gson gson = new Gson();
        NoteManager.getInstance().init(sharedPreferences, gson);
    }
}
