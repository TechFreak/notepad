package co.juliansuarez.notepad.data.network.model;

/**
 * Created by jsuarez on 1/12/16.
 */
public class ExternalNote {
    private String id;
    private String content;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
