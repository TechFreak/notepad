package co.juliansuarez.notepad.data.network;

import java.util.List;

import co.juliansuarez.notepad.data.network.model.ExternalNote;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by jsuarez on 1/12/16.
 */
public interface Api {

    @GET("/items")
    Call<List<ExternalNote>> getItems();
}
