package co.juliansuarez.notepad.data;

import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import co.juliansuarez.notepad.model.Note;

/**
 * Created by Julián on 18/11/2015.
 */
public class NoteManager {

    public static final String NOTES_PREFERENCES = "notes_prefs";
    public static final String TAG_NOTES = "notes";
    private static final String TAG_ID_COUNTER = "id_counter";
    private static NoteManager instance;
    private SharedPreferences sharedPreferences;
    private Gson gson;
    private List<Note> notes;

    private NoteManager() {

    }

    public static NoteManager getInstance() {
        if (instance == null) {
            instance = new NoteManager();
        }

        return instance;
    }

    public void init(SharedPreferences sharedPreferences, Gson gson) {
        this.sharedPreferences = sharedPreferences;
        this.gson = gson;
    }

    public List<Note> getNotes() {
        if (notes == null) {
            notes = loadNotes();
        }
        return notes;
    }

    private List<Note> loadNotes() {
        final String notesString = sharedPreferences.getString(TAG_NOTES, "");
        Type listofNotesType = new TypeToken<List<Note>>() {
        }.getType();
        List<Note> notes = gson.fromJson(notesString, listofNotesType);
        if (notes == null) {
            notes = new ArrayList<>();
        }
        return notes;
    }

    public Note saveNote(Note note) {
        final List<Note> notes = getNotes();
        if (alreadyAddedExternalNote(note)) {
            // Skip don't save anything
        } else if (note.getId() == 0) {
            // New note add to our list
            note.setId(getNextId());
            note.setCreationDate(new Date());
            notes.add(note);
        } else {
            // Updating an existing note
            int index = notes.indexOf(note);
            if (index == -1) {
                throw new InvalidParameterException("Invalid note id");
            } else {
                notes.get(index).setIsFavorite(note.isFavorite());
                notes.get(index).setContent(note.getContent());

            }
        }

        saveNotes();
        return note;
    }

    private boolean alreadyAddedExternalNote(Note note) {
        if (note.getExternalId() != 0) {
            for (Note n : notes) {
                if (n.getExternalId() != 0 && n.getExternalId() == note.getExternalId()) {
                    return true;
                }
            }
        }
        return false;
    }

    private void saveNotes() {
        // Sort notes
        Collections.sort(notes, new Comparator<Note>() {
            @Override
            public int compare(Note lhs, Note rhs) {
                if (lhs.isFavorite() == rhs.isFavorite()) {
                    // Sort by date
                    return lhs.getCreationDate().compareTo(rhs.getCreationDate());
                } else if (lhs.isFavorite() && !rhs.isFavorite()) {
                    return -1;
                } else {
                    return 1;
                }
            }
        });

        // Save notes
        final SharedPreferences.Editor edit = sharedPreferences.edit();
        final String notesJson = gson.toJson(notes);
        edit.putString(TAG_NOTES, notesJson);
        edit.commit();
    }

    private long getNextId() {
        long idCounter = sharedPreferences.getLong(TAG_ID_COUNTER, 0L);
        idCounter++;
        final SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putLong(TAG_ID_COUNTER, idCounter);
        edit.commit();
        return idCounter;
    }

    public List<Note> search(String searchTerm) {
        List<Note> results = new ArrayList<>();
        final List<Note> notes = getNotes();
        for (Note n : notes) {
            if (n.getContent().toLowerCase().contains(searchTerm.trim().toLowerCase())) {
                results.add(n);
            }
        }
        return results;
    }

    public void deleteNote(Note note) {
        if (note.getId() == 0) {
            throw new InvalidParameterException("Invalid note id");
        }
        final List<Note> notes = getNotes();
        notes.remove(note);
        saveNotes();
    }
}
