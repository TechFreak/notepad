package co.juliansuarez.notepad.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.test.AndroidTestCase;
import android.test.mock.MockContext;
import android.test.suitebuilder.annotation.MediumTest;

import com.google.gson.Gson;

import java.util.List;

import co.juliansuarez.notepad.model.Note;

/**
 * Created by Julián on 18/11/2015.
 */
public class NoteManagerTest extends AndroidTestCase {
    NoteManager noteManager;

    @Override
    public void setUp() throws Exception {
        super.setUp();
        Context c = new DelegatedMockContext(getContext());
        final SharedPreferences sharedPreferences = c.getSharedPreferences("notes", Context.MODE_PRIVATE);
        sharedPreferences.edit().putString(NoteManager.TAG_NOTES, "").commit();
        noteManager = new NoteManager(sharedPreferences, new Gson());
    }

    @MediumTest
    public void testGetNotes() throws Exception {
        noteManager.saveNote(new Note("1"));
        noteManager.saveNote(new Note("2"));
        assertTrue("Incorrect number of saved notes", noteManager.getNotes().size() == 2);
        noteManager.saveNote(new Note("3"));
        assertTrue("Incorrect number of saved notes", noteManager.getNotes().size() == 3);
    }

    public void testSaveNote() throws Exception {
        Note note = noteManager.saveNote(new Note("Test"));
        assertTrue("There must be one saved note", noteManager.getNotes().size() == 1);
        assertTrue("Id not set", note.getId() != 0L);
    }

    public void testSearch() throws Exception {
        noteManager.saveNote(new Note("test foo"));
        noteManager.saveNote(new Note("TEST BAR"));
        List<Note> searchResults = noteManager.search("te");
        assertTrue("There must be 2 search results", searchResults.size() == 2);
        searchResults = noteManager.search("ST");
        assertTrue("There must be 2 search results", searchResults.size() == 2);
        searchResults = noteManager.search("foo");
        assertTrue("There must be one search result", searchResults.size() == 1);
        searchResults = noteManager.search("test a");
        assertTrue("There must be 0 search results", searchResults.size() == 0);
        searchResults = noteManager.search("");
        assertTrue("There must be 2 search results", searchResults.size() == 2);
    }

    public void testDeleteNote() throws Exception {
        Note note = noteManager.saveNote(new Note("Test"));
        assertTrue("There must be one saved note", noteManager.getNotes().size() == 1);
        noteManager.deleteNote(note);
        assertTrue("Note was not deleted correctly", noteManager.getNotes().size() == 0);
    }

    class DelegatedMockContext extends MockContext {

        private Context mDelegatedContext;
        private static final String PREFIX = "test.";

        public DelegatedMockContext(Context context) {
            mDelegatedContext = context;
        }

        @Override
        public String getPackageName() {
            return PREFIX;
        }

        @Override
        public SharedPreferences getSharedPreferences(String name, int mode) {
            return mDelegatedContext.getSharedPreferences(name, mode);
        }
    }
}
